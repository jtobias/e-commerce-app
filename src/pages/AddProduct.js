import { useState, useEffect, useContext } from 'react';
import { Form, Button, InputGroup } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function NewProduct() {


	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price, setPrice ] = useState("");
	const navigate = useNavigate();


	// Add the product to database
		function addProduct(e) {

			e.preventDefault();


			fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
				method : "POST",
				headers : {
					"Content-Type" : "application/json",
					"Authorization" : `Bearer ${ localStorage.getItem('token') }`
				},
				body : JSON.stringify({
					name : name,
					description : description,
					price: price
				})
			})
			.then(res => res.json())
			.then(data => {
				//console.log(data)
				if (data === true) {

					Swal.fire({
						icon : "success",
						title : "Added successfully",
						text : "Product is now added and available to customers"
					}).then((result) => {
							navigate("/admin");
					})

				

				} else {

					Swal.fire({
						icon : "error",
						title : "Something went wrong",
						text : "Please try again. Note that Product Name should be unique. Try adding a number on the Product Name"
					});

				}
			})

			

		}
	// End Add the product to database



	// useEffect for Add Product Button
		const [ isActive, setIsActive ] = useState(false); 

		useEffect(() => {

			if ((name !== "" && description !== "" && price !== "") && (price > 0) && (price < 1000000001)) {
				setIsActive(true);
			} else {
				setIsActive(false)
			}
		}, [ name, description, price])
	// End useEffect for Add Product Button

	
	return (

		<div className="d-flex align-items-center justify-content-center mt-5 text-muted">
			<Form 
				onSubmit={ (e) => addProduct(e) }
				className="bg-light m-5 p-5 w-50"
				style={{  
				    backgroundImage: "url(https://3.bp.blogspot.com/-5b_M2KGymwQ/Uimxq074HbI/AAAAAAAACr8/eT93WgQaoDI/s1600/shaded+square+1.jpg)",
				    backgroundRepeat: "no repeat",
				    backgroundSize: '110%',
				    backgroundPosition: 'center',
				}}
			>

				<h2 className="mt-3">Add a New Product</h2>

				<Form.Group className="mt-3">
					<Form.Label>Product Name</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Product Name here"
						value={ name }
						onChange={ e => setName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group className="mt-3">
					<Form.Label>Description</Form.Label>
					<Form.Control 
						as="textarea"
						rows={3}
						placeholder="Enter Product Description here"
						value={ description }
						onChange={ e => setDescription(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group className="mt-3">
					<Form.Label>Price</Form.Label>
					<InputGroup>
						<InputGroup.Text>₱</InputGroup.Text>
						<Form.Control 
							type="number"
							placeholder="Set price"
							value={ price }
							onChange={ e => setPrice(e.target.value)}
							required
						/>
					</InputGroup>
				</Form.Group>

				{
					isActive ? 
						<Button variant="warning" type="submit" id="submitBtn" className="mt-5">Add Product</Button>
						:
						<Button variant="warning" type="submit" id="submitBtn" className="mt-5" disabled>Add Product</Button>
				}

			</Form>
		</div>
	)

}