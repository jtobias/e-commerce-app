import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register() {

	const { user, setUser } = useContext(UserContext);
	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [ email, setEmail ] = useState("");
	const [ password1, setPassword1 ] = useState("");
	const [ password2, setPassword2 ] = useState("");
	const [ firstName, setFirstName ] = useState("");
	const [ lastName, setLastName ] = useState("");
	const [ mobileNumber, setMobileNumber ] = useState("");
	
	// State to determine whether the submit buttong is enabled or not
	const [ isActive, setIsActive ] = useState(false);


	function registerUser(e) {

		
		// register user in database
		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method : "POST",
			headers : {
				"Content-Type" : "application/json"
			},
			body : JSON.stringify({
				firstName : firstName,
				lastName : lastName,
				email : email,
				mobileNo : mobileNumber,
				password : password1,
			})
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);

			if (data === true) {

				Swal.fire({
					icon : "success",
					title : "Registration successful",
					text : "Welcome to Zuitt!"
				}).then((result) => {
					navigate("/login");
				})

			} else {

				Swal.fire({
						icon : "error",
						title : "Duplicate email found",
						text : "Please provide a different email."
					});
			}

		})

		

		// page will automatically redirect to login page once registered
		// navigate("/login");

			

		// prevents page redirection via form submission
		e.preventDefault();

		// clears all the information on the form
		setEmail("");
		setPassword1("");
		setPassword2("");
		setFirstName("");
		setLastName("");
		setMobileNumber("");



		// alert("Thank you for registering!");
	}


	useEffect(() => {



		// Validation to enable submit button when all fields are populated and both passwords match
		if ((email !== "" && password1 !== "" && password2 !== "" && firstName !== "" && lastName !== "" && mobileNumber !== "") && (password1 === password2) && (mobileNumber.length === 11 )) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [ email, password1, password2, firstName, lastName, mobileNumber ]);


	return(

		// (user.id) ?
		// 	<Navigate to="/" />
		// :
			<div className="d-flex align-items-center justify-content-center mt-5 text-muted">
				<Form 
					onSubmit={ (e) => registerUser(e) }
					className="bg-light m-5 p-5 w-50"
					style={{  
					    backgroundImage: "url(https://3.bp.blogspot.com/-5b_M2KGymwQ/Uimxq074HbI/AAAAAAAACr8/eT93WgQaoDI/s1600/shaded+square+1.jpg)",
					    backgroundRepeat: "no repeat",
					    backgroundSize: '110%',
					    backgroundPosition: 'center',
					}}
				>
					<h2 className="text-center text-muted">Register</h2>
					<Form.Group controlId="firstName">
						<Form.Label>First Name</Form.Label>
						<Form.Control 
							type="text"
							placeholder="Enter first name"
							value={ firstName }
							onChange={ e => setFirstName(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="lastName" className="mt-3">
						<Form.Label>Last Name</Form.Label>
						<Form.Control 
							type="text"
							placeholder="Enter last name"
							value={ lastName }
							onChange={ e => setLastName(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="userEmail" className="mt-3">
						<Form.Label>Email Address</Form.Label>
						<Form.Control 
							type="email"
							placeholder="email@mail.com"
							value={ email }
							onChange={ e => setEmail(e.target.value)}
							required
						/>
						<Form.Text className="text-muted">We'll never share your email with anyone.</Form.Text>
					</Form.Group>


					<Form.Group controlId="mobileNumber" className="mt-3">
						<Form.Label>Mobile Number</Form.Label>
						<Form.Control 
							maxLength={11}
							type="tel"
							placeholder="09XXXXXXXXX"
							value={ mobileNumber }
							onChange={ e => setMobileNumber(e.target.value)}
							required
						/>
					</Form.Group>
					
					<Form.Group controlId="password1" className="mt-3">
						<Form.Label>Password</Form.Label>
						<Form.Control 
							type="password"
							placeholder="Enter a password"
							value={ password1 }
							onChange={ e => setPassword1(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="password2" className="mt-3">
						<Form.Label>Confirm Password</Form.Label>
						<Form.Control 
							type="password"
							placeholder="Confirm your password"
							value={ password2 }
							onChange={ e => setPassword2(e.target.value)}
							required
						/>
					</Form.Group>


					{
						isActive ? 
							<Button variant="warning" type="submit" id="submitBtn" className="mt-4">Register</Button>
							:
							<Button variant="warning" type="submit" id="submitBtn" className="mt-4" disabled>Register</Button>
					}

				</Form>
			</div>
	)
}