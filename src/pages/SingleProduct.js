import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, InputGroup, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function SingleProduct() {
	
	const { productId } = useParams();
	const { user } = useContext(UserContext);

	const [ productName, setProductName ] = useState("");
	const [ quantity, setQuantity ] = useState("");
	const [ totalAmount, setTotalAmount ] = useState("");

	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price, setPrice ] = useState("");


	const navigate = useNavigate();
	const [ isActive, setIsActive ] = useState(false);


	function order(e) {

		

		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method : "POST",
			headers : {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${ localStorage.getItem('token') }`
			},
			body : JSON.stringify({
				productId : productId,
				productName : productName,
				quantity : quantity,
				totalAmount : price*quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			if (data === true) {

				Swal.fire({
					icon : "success",
					title : "Checkout Succesful",
					text : "Item is now ordered"
				}).then((result) => {
					navigate("/products");
				})

			} else {

				Swal.fire({
					icon : "error",
					title : "Something went wrong",
					text : "Please try again."
				});

			}
		})

	}


	useEffect(() => {
		fetch(`https://capstone-2-1jj6.onrender.com/products/${ productId }`)
			.then(res => res.json())
			.then(data => {

				//console.log(data)
				setName(data.name);
				setProductName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				// setTotalAmount(data.price*quantity)
			})
	}, [productId])




	useEffect(() => {

		if((quantity > 0) && (quantity <100)) {

			setIsActive(true);
		} else {
			setIsActive(false)
		}
	}, [quantity])


	return(

		<Container 
			className="mt-5"
			
		>
			<Row>
				<Col lg={{ span : 6, offset : 3}} >
					<Card 
						style={{  
			                backgroundImage: "url(https://cdn1.vectorstock.com/i/1000x1000/04/30/back-to-school-background-vector-26570430.jpg)",
			                backgroundRepeat: "no repeat",
			                backgroundSize: '150%',
			                backgroundPosition: 'center',
            			}}
            			className="text-light"
            		>

						<Card.Body className="text-light">
							<Card.Title className="text-warning mt-3"> { name } </Card.Title>
							<Card.Subtitle className="mt-3">Description:</Card.Subtitle>
							<Card.Text className="mt-1 col-8"  > { description } </Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text> ₱ { price } </Card.Text>
							<Card.Subtitle>Total Amount:</Card.Subtitle>
							<Card.Text onChange={ e => setTotalAmount(e.target.value)}> ₱ { price*quantity } </Card.Text>
							{/*<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text> 8am - 5pm </Card.Text>*/}
							
							{/*<Form.Group>
								<InputGroup>
									<InputGroup.Text>Quantity</InputGroup.Text>
									<Form.Control
										type="number"
										placeholder="Enter values between 0-99"
										min="1"
										max="99"
										required
										value={ quantity }
										onChange={ e => setQuantity(e.target.value)}
									/>
								</InputGroup>
							</Form.Group>*/}
							<Card.Subtitle> Quantity: </Card.Subtitle>
							<input className="text-center mt-2 col-1"
								//type="number"
								type="tel"
								placeholder="1-99"
								//min="1"
								//max="99"
								maxLength={2}
								//step="1"
								required
								value={ quantity }
								onChange={ e => setQuantity(e.target.value)}
							/><br/>

							{/*<Card.Subtitle className="mt-3">Total Amount:</Card.Subtitle>
							<input 
							 	type="number"
								placeholder="1"
								min="1"
								max="99"
								readonly
								// disabled
								required
								value={ price * quantity }
								onChange={ e => setTotalAmount(e.target.value)}
							/><br/>*/}
							{
								user.id ?

									user.isAdmin ?
										<>
											<Button className="mt-3" variant="secondary" disabled>Order</Button>
											<p className="text-warning mt-2 col-6">Please login using your Non-Admin account to order.</p>
										</>
									:

										isActive ?

											<Button className="mt-3" variant="secondary" block onClick={() => order(productId)}>Order</Button>
										:

											<Button className="mt-3" variant="secondary" block onClick={() => order(productId)} disabled>Order</Button>


								:
									<Link className="btn btn-secondary btn-block mt-3" to="/login">Log in to Order</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>


	)

}