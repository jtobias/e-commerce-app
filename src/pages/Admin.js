import { useEffect, useState, useContext } from 'react';
import { Table, Container, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import AdminTable from '../components/AdminTable';
import UserContext from '../UserContext';

export default function Admin() {

	const [ products, setProducts ] = useState([]);
	const { user } = useContext(UserContext);

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers : {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${ localStorage.getItem('token') }`
			}
		})
			.then(res => res.json())
			.then(data => {

				//console.log(data);

				
				setProducts(data.map(product => {

					return (

						<AdminTable key={ product._id } product = { product } />
					)
				}));
			})
	}, []);


	return (

		<Container className="text-center">
			<h2 className="mt-5">Admin Dashboard</h2>
			<Link className = "m-3 btn btn-warning" to="/addproduct">Add New Product</Link>
			{/*<Link className = "m-3 btn btn-warning" to="/showuserorders" disabled>Show User Orders</Link>*/}


			<Table striped bordered size="sm" align="middle" className="mt-3">
				<thead>
					<tr>
						<th className="bg-dark text-warning align-middle">Name</th>
						<th className="bg-dark text-warning align-middle">Description</th>
						<th className="bg-dark text-warning align-middle">Price per item</th>
						<th className="bg-dark text-warning align-middle">Availability</th>
						<th className="bg-dark text-warning align-middle">Actions</th>
					</tr>
				</thead>
				<tbody>
					{ products }
				</tbody>
			</Table>
		</Container>
	)


};


