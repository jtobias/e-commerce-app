import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	
	const { user, setUser } = useContext(UserContext);

	const [ email, setEmail ] = useState("");
	const [ password, setPassword ] = useState("");
	const [ isActive, setIsActive ] = useState(false);

	const navigate = useNavigate();

	useEffect(() => {

		if (email !== "" && password !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [ email, password ])



	function loginUser(e) {
		
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method : "POST",
			headers : {
				"Content-Type" : "application/json"
			},
			body : JSON.stringify({
				email : email,
				password : password
			})
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);

			if(typeof data.access !== "undefined"){

				localStorage.setItem("token", data.access);
				//console.log(data.access)
				retrieveUserDetails(data.access);

				Swal.fire({
					icon: 'success',
					title: 'Login successful!',
					text: 'Welcome to Zuitt!'
				})
				// .then((result) => {
				// 			navigate("/products");
				// 	})


			} else {
				Swal.fire({
					icon: 'error',
					title: 'Authenticaton failed',
					text: 'Please check your login credentials and try again.'
				})
			}
		});

		
		setEmail("");
		setPassword("");

		return true;

	}



	const retrieveUserDetails = (token) => {

		fetch("https://capstone-2-1jj6.onrender.com/users/profile", {
			headers : {
				Authorization : `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then(data => {

			//console.log(data);

			localStorage.setItem("isAdmin", data.isAdmin);

			setUser({
				id : data._id,
				isAdmin : data.isAdmin
			})
		})
	}



	return (

		(user.id) ?
			<Navigate to="/" />
		:
			<div className="d-flex align-items-center justify-content-center mt-5">
				<Form 
					onSubmit={ (e) => loginUser(e) } 
					className="bg-light m-5 p-5 w-50"
					style={{  
					    backgroundImage: "url(https://3.bp.blogspot.com/-5b_M2KGymwQ/Uimxq074HbI/AAAAAAAACr8/eT93WgQaoDI/s1600/shaded+square+1.jpg)",
					    backgroundRepeat: "no repeat",
					    backgroundSize: '110%',
					    backgroundPosition: 'center',
					}}
				>
					<h2>Login</h2>

					<Form.Group>
						<Form.Label>Email Address</Form.Label>
						<Form.Control 
							type="email"
							placeholder="Enter email"
							value={ email }
							onChange={ e => setEmail(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control 
							type="password"
							placeholder="Password"
							value={ password }
							onChange={ e => setPassword(e.target.value)}
							required
						/>
					</Form.Group>

					{
						isActive ?
							<Button variant="warning" className="mt-3" type="submit">Login</Button>
							:
							<Button variant="warning" className="mt-3" type="submit" disabled>Login</Button>
					}
					
				</Form>
			</div>

	)
}

//https://3.bp.blogspot.com/-5b_M2KGymwQ/Uimxq074HbI/AAAAAAAACr8/eT93WgQaoDI/s1600/shaded+square+1.jpg