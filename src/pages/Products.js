import { useEffect, useState } from 'react';
import { Row, Col, Card, Container, Stack } from 'react-bootstrap';

import ProductCard from '../components/ProductCard';


export default function Products(product) {

	const [ products, setProducts ] = useState([]);

	useEffect((product) => {

		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
			.then(res => res.json())
			.then(data => {

				//console.log(data);
				setProducts(data.map(product => {
					return (

							
								<ProductCard key={ product._id } product = { product } />
							
						
					)
				}))
			})
	}, [])

	return (
		
			<div className="d-flex flex-wrap justify-content-center">
				{ products }
			</div>
		
	)

}