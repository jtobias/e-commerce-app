
import { useEffect, useState } from 'react';


import OrderCard from '../components/OrderCard';



export default function Orders(order) {
	
	const [ orders, setOrders ] = useState([])

	useEffect((order) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/myorders`, {
			headers : {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${ localStorage.getItem('token') }`
			}
		}).then(res => res.json())
			.then(data => {

				//console.log(data);
				setOrders(data.map(order => {

					return (

						<OrderCard key={ order._id} order = { order } />
					)
				}))
			})


	}, [])


	return(
		<div className="d-flex flex-wrap justify-content-center">
			{ orders }
		</div>
	)

}