import { Button, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom'



export default function Banner() {

	return (

		<div className="d-flex align-items-center justify-content-center mt-5" bg="dark">
			<div className="p-5 text-center mt-5 bg-light">
				<h1>Welcome to Supply Kit</h1>
				<p className="banner-text">Your online school and office supplies store</p>
				<Link className="btn btn-warning" to="/products">
					Buy Now!
				</Link>
			</div>
		</div>
	)
}
