import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';

export default function AppNavbar() {

	const { user } = useContext(UserContext);

	return (

		<Navbar bg="dark" expand="lg" data-bs-theme="dark">
			<Container fluid>
				<Navbar.Brand as={ NavLink } to="/" className="text-warning ms-5">Supply Kit</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto me-5">
						<Nav.Link className="text-light me-3" as={ NavLink } to="/">Home</Nav.Link>
						<Nav.Link className="text-light me-3" as={ NavLink } to="/products">Products</Nav.Link>
						{
							(user.id) ?

								user.isAdmin ?
									<>
										<Nav.Link className="text-light me-3" as={ NavLink } to="/admin">Admin</Nav.Link>
										<Nav.Link className="text-light me-3" as={ NavLink } to="/logout">Logout</Nav.Link>
									</>
								:
									<>
										<Nav.Link className="text-light me-3" as={ NavLink } to="/orders">Orders</Nav.Link>
										<Nav.Link className="text-light me-3" as={ NavLink } to="/logout">Logout</Nav.Link>
									</>

							:
							<>
								<Nav.Link className="text-light me-3" as={ NavLink } to="/login">Login</Nav.Link>
								<Nav.Link className="text-light me-3" as={ NavLink } to="/register">Register</Nav.Link>
							</>
						}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>

	)
}