import { Row, Col, Card, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';


export default function OrderCard({order}) {

    const {  _id, purchasedOn, totalAmount, products, products: [{productName, quantity }] } = order;

    //console.log(order)

    // let purchaseDate = mb_substr(purchasedOn, 0, 511);
    let purchaseDate = purchasedOn.substring(0, 10);



	return (

		<Card 
            className="m-5 text-light"
            style={{ 
                width: '18rem', 
                backgroundImage: "url(https://cdn1.vectorstock.com/i/1000x1000/04/30/back-to-school-background-vector-26570430.jpg)",
                backgroundRepeat: "no repeat",
                backgroundSize: '150%',
                backgroundPosition: 'center',
            }}
        >
            <Card.Body>
                <Card.Title className="mt-3 text-center text-warning">{ productName }</Card.Title>
                <Card.Subtitle>Quantity</Card.Subtitle>
                <Card.Text>{ quantity }</Card.Text>

                <Card.Subtitle>Total Amount</Card.Subtitle>
                <Card.Text>₱ { totalAmount }</Card.Text>

                <Card.Subtitle>Date of Purchase:</Card.Subtitle>
                <Card.Text>{ purchaseDate }</Card.Text>
                
                {/*<Card.Text>Enrollees : { count }</Card.Text>
                <Card.Text>Seats Available : { seats }</Card.Text>*/}
                {/*<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>*/}
            </Card.Body>
        </Card>	

	)
}