import { Row, Col, Card, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';


export default function ProductCard({product}) {

    const { _id, name, description, price } = product;


	return (

		<Card 
            className="m-5 text-light" 
            //bg="dark"
            //style={{ background: "radial-gradient( circle farthest-corner at 10% 20%,  rgba(37,145,251,0.98) 0.1%, rgba(0,7,128,1) 99.8% )", width: '18rem' }}
            style={{ 
                width: '18rem', 
                backgroundImage: "url(https://cdn1.vectorstock.com/i/1000x1000/04/30/back-to-school-background-vector-26570430.jpg)",
                backgroundRepeat: "no repeat",
                backgroundSize: '200%',
                backgroundPosition: 'center',
            }}
            // https://backiee.com/static/wpdb/wallpapers/1000x563/230607.jpg

        >
            <Card.Body>
                <Card.Title className="mt-3 text-center text-warning">{ name }</Card.Title>
                <Card.Subtitle className="mt-3" >Description:</Card.Subtitle>
                <Card.Text className="mt-1">{ description }</Card.Text>
                <Card.Subtitle className="mt-3">Price:</Card.Subtitle>
                <Card.Text className="mt-1">₱ { price }</Card.Text>
                {/*<Card.Text>Enrollees : { count }</Card.Text>
                <Card.Text>Seats Available : { seats }</Card.Text>*/}
                <div className="d-flex align-items-center justify-content-center">
                    <Link className="btn mt-3 btn-secondary" to={`/products/${_id}`}>Details</Link>
                </div>
            </Card.Body>
        </Card>	

	)
}