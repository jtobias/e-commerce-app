import { useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UpdateProduct from '../pages/UpdateProduct';


export default function AdminTable({product}) {


	//console.log(product);
	//console.log(typeof product);

	const { _id, name, description, price, isActive } = product;
	const { productId } = useParams();



	// Disable

		
		// const navigate = useNavigate();

		const disable = (productId) => {

			fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }/archive`, {
				method : "PATCH",
				headers : {
					"Content-Type" : "application/json",
					Authorization : `Bearer ${ localStorage.getItem('token') }`
				},
				body : JSON.stringify({
					productId : productId
				})
			})
				.then(res => res.json())
				.then(data => {
					//console.log(data)

					if(data === true) {

						Swal.fire({
							icon : "success",
							title : "Successfully disabled",
							text : "You have successfully disabled this Product and will no longer be available to users"
						}).then((result) => {
							location.reload();
						})

						


					} else {

						Swal.fire({
							icon : "error",
							title : "Something went wrong",
							text : "Please try again."
						}).then((result) => {
							location.reload();
						})

						
					}

					
				})

				// navigate("/");
				// navigate("/admin");
				// location.reload();

		}

	// End Disable


	// Enable

		// const { productId } = useParams();

		const enable = (productId) => {

			fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }/activate`, {
				method : "PATCH",
				headers : {
					"Content-Type" : "application/json",
					Authorization : `Bearer ${ localStorage.getItem('token') }`
				},
				body : JSON.stringify({
					productId : productId
				})
			})
				.then(res => res.json())
				.then(data => {
					//console.log(data)

					if(data === true) {

						Swal.fire({
							icon : "success",
							title : "Successfully enabled",
							text : "You have successfully enabled this Product and will now be available to users"

						}).then((result) => {
							location.reload();
						})

						

						

					} else {

						Swal.fire({
							icon : "error",
							title : "Something went wrong",
							text : "Please try again."
						}).then((result) => {
							location.reload();
						})

						
					}

					

				})

				// navigate("/");
				// navigate("/admin");
				// location.reload();

		}

	// End Enable


	// useEffect for product.isActive

		// const [ isProductActive, setIsProductActive ] = useState();

		// useEffect(() => {

		// 	if (product.isActive) {

		// 		setIsProductActive(true)
		// 	} else {

		// 		setIsProductActive(false)
		// 	}
		// }, [product.isActive]) 


	// End useEffect for product.isActive

	// Availability message conversion

		const [ isAvailable, setisAvailable ] = useState("");

		useEffect (() => {
			if (product.isActive === true) {
				setisAvailable(`Available`) 
			} else {
				setisAvailable(`Not Available`) 
			}

		}, [isAvailable])


		// const { productId } = useParams();
		// const [ useName, setName ] = useState("");
		// const [ useDescription, setDescription ] = useState("");
		// const [ usePrice, setPrice ] = useState("");
		// const [ useisActive, setIsActive ] = useState("");



		// useEffect(() => {

		// 	console.log(productId)

		// 	fetch(`https://capstone-2-1jj6.onrender.com/products/${ productId }`)
		// 		.then(res => res.json())
		// 		.then(data => {

		// 			setName(data.name);
		// 			setDescription(data.description);
		// 			setPrice(data.price);
		// 			setIsActive(data.isActive)
		// 		})
		// }, [productId])

	// End Availability message conversion




		return(

			
					<tr>
						<td className="align-middle">{name}</td>
						<td className="align-middle">{description}</td>
						<td className="align-middle">₱ {price}</td>
						<td className="align-middle">{isAvailable}</td>
						<td className="align-middle">
							<Link className="btn btn-warning mt-2" to={`/products/${_id}/update`}>Update</Link>
							{
								(product.isActive) ?
									<Button variant="danger" className="mt-2 mb-2" onClick={() => disable(product._id)}>Disable</Button>
								:
									<Button variant="success" className="mt-2 mb-2" onClick={() => enable(product._id)}>Enable</Button>
							}

						</td>
					</tr>

		)


}