import { BrowserRouter as Router } from 'react-router-dom';
import { Container } from 'react-bootstrap'
import { Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';


import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import AddProduct from './pages/AddProduct';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Admin from './pages/Admin';
import UpdateProduct from './pages/UpdateProduct';
import SingleProduct from './pages/SingleProduct';
import Orders from './pages/Orders';
import ShowUserOrders from './pages/ShowUserOrders';

import './App.css';
import { UserProvider } from './UserContext';

function App() {


  const [ user, setUser ] = useState({ id : null, isAdmin : null })

  // to clear localStorage on logout
  const unsetUser = () => {
      localStorage.clear();
  }


  useEffect(() => {

    if(localStorage.getItem("token")) {

        fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
           headers : {
             Authorization : `Bearer ${ localStorage.getItem("token") }`
           }
         })
         .then(res => res.json())
         .then(data => {

           //console.log(data);


           // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
             
             // if(data.id){
                 setUser({
                   id : data._id,
                   isAdmin : data.isAdmin
                 })
             // }
         });
    }

      //console.log(user);
      //console.log(localStorage);
    }, []);

  return (
    
      <UserProvider value={{ user, setUser, unsetUser }}>
          <Router >
                  <AppNavbar />
                  <Container>
                      <Routes>
                      <Route path="/" element={<Home />} />
                          <Route path="/products" element={<Products />} />
                          {/*<Route path="/courses/:courseId" element={<CourseView />} />*/}
                          <Route path="/register" element={<Register />} />
                          <Route path="/login" element={<Login />} />
                          <Route path="/admin" element={<Admin />} />
                          <Route path="/orders" element={<Orders />} />
                          <Route path="/logout" element={<Logout />} />
                          <Route path="/addproduct" element={<AddProduct />} />
                          {/*<Route path="/showuserorders" element={<ShowUserOrders />} />*/}
                          <Route path="/products/:productId/update" element={<UpdateProduct />} />
                          <Route path="/products/:productId" element={<SingleProduct />} />
                          {/*<Route path="/*" element={<PageNotFound />} />*/}
                     </Routes>
                  </Container>
          </Router>
      </UserProvider>


  );
}

export default App;
